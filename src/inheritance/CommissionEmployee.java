package inheritance;

/**
 * @author rajim 2022-04-29.
 * @project IntelliJ IDEA
 */

public class CommissionEmployee {

    private String name;

    private String employeeId;

    private double rate;

    private double totalSales;


    public double totalEarning() {
        return getRate() * getTotalSales();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(double totalSales) {
        this.totalSales = totalSales;
    }

    @Override
    public String toString() {
        return "Baisc Employee Name::" + getName()
                + " EmployeeId::"
                + getEmployeeId()
                + "Rate::" + getRate()
                + " Total Sales::"
                + getTotalSales();
    }
}
