package inheritance;

/**
 * @author rajim 2022-04-29.
 * @project IntelliJ IDEA
 */
public class BasicPlusCommissionEmployee
        extends CommissionEmployee {
    private double basicSalary;

    @Override
    public double totalEarning() {
         return super.totalEarning() + getBasicSalary();
    }

    public double getBasicSalary() {
        return basicSalary;
    }

    public void setBasicSalary(double basicSalary) {
        this.basicSalary = basicSalary;
    }

    @Override
    public String toString() {
        return super.toString()+"BasicPlusCommissionEmployee{" +
                "basicSalary=" + basicSalary +
                '}';
    }
}
