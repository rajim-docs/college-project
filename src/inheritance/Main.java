package inheritance;

/**
 * @author rajim 2022-04-29.
 * @project IntelliJ IDEA
 */
public class Main {
    public static void main(String[] args) {

        BasicPlusCommissionEmployee basicPlusCommissionEmployee = new BasicPlusCommissionEmployee();
        basicPlusCommissionEmployee.setName("test");
        basicPlusCommissionEmployee.setBasicSalary(1000);
        basicPlusCommissionEmployee.setEmployeeId("123");
        basicPlusCommissionEmployee.setRate(2);
        basicPlusCommissionEmployee.setTotalSales(100);

        System.out.println("Basic Employee Detail::"+
                basicPlusCommissionEmployee.toString());

        System.out.println("Total Earning"
                +basicPlusCommissionEmployee.totalEarning());

//        CommissionEmployee commissionEmployee = new CommissionEmployee();
//        commissionEmployee.setSa

    }
}
