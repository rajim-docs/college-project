package settergetter;

import java.util.ArrayList;
import java.util.*;

/**
 * @author rajim 2022-04-22.
 * @project IntelliJ IDEA
 */
public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter total number of employee::");
        int number = scanner.nextInt();

        Employee employee = new Employee();

        Employee employeeData =
                employee.readEmployeeData();
        System.out.println("Employeename::"+employeeData.getFullName());

        List<Employee> employeeList =
                employee.readEmployeeList(number);

        System.out.println("Single employee::"+employeeData.toString());

        System.out.println("List of Employee::"+employeeList.toString());

    }
}
