package settergetter;

import java.util.*;

/**
 * @author rajim 2022-04-22.
 * @project IntelliJ IDEA
 */
public class Employee {

    private String fullName;

    private String address;

    private double salary;

    @Override
    public String toString() {
        return "Employee{" +
                "fullName='" + fullName + '\'' +
                ", address='" + address + '\'' +
                ", salary=" + salary +
                '}';
    }

//    public static String getEmployeeName() {
//        return "Hello";
//    }

    // read employeee
    public Employee readEmployeeData() {
        Employee employee = new Employee();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Full Name:: ");
//            String fullName = scanner.next();
        employee.setFullName(scanner.next());

        System.out.println("Enter Address");
        employee.setAddress(scanner.next());

        System.out.println("Enter Salary::");
        employee.setSalary(scanner.nextDouble());
        return employee;
    }

    // Read employee List
    public List<Employee> readEmployeeList(int number) {
        List<Employee> employeeList = new ArrayList<>();
        for(int i = 0; i< number; i++) {
            employeeList.add(readEmployeeData());
        }
        return employeeList;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return this.fullName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return this.address;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getSalary() {
        return this.salary;
    }
}
