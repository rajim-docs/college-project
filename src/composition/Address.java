package composition;

import java.util.Scanner;

/**
 * @author rajim 2022-04-25.
 * @project IntelliJ IDEA
 */
public class Address {

    private String temporaryAddress;

    private String permanentAddress;

    @Override
    public String toString() {
        return "Address{" +
                "temporaryAddress='" + temporaryAddress + '\'' +
                ", permanentAddress='" + permanentAddress + '\'' +
                '}';
    }

    public Address getAddressData() {
        Scanner scanner = new Scanner(System.in);
        Address address = new Address();
        System.out.println("Enter your Temporary Address::");
        address.setTemporaryAddress(scanner.next());
        System.out.println("Enter your permanent address::");
        address.setPermanentAddress(scanner.next());
        return address;
    }

    public String getTemporaryAddress() {
        return temporaryAddress;
    }

    public void setTemporaryAddress(String temporaryAddress) {
        this.temporaryAddress = temporaryAddress;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }
}
