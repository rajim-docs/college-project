package composition;

import java.util.*;
/**
 * @author rajim 2022-04-25.
 * @project IntelliJ IDEA
 */
public class Student {

    private String name;

    private String rollNumber;

    private Address address;

    private List<Contacts> contacts;

    public Student readStudentData() {
        Scanner scanner = new Scanner(System.in);
        Student student = new Student();
        System.out.println("Enter Student Name::");
        student.setName(scanner.next());
        try {
            System.out.println("Enter Student's roll number::");
            student.setRollNumber(scanner.next());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Enter Student's roll number::");
            student.setRollNumber(scanner.next());
        }

        // read address
        Address address = new Address();
        Address addressData = address.getAddressData();
        student.setAddress(addressData);

        // read contacts
        System.out.println("Enter number of contacts::");
        int number = scanner.nextInt();
        Contacts contacts = new Contacts();

        List<Contacts> contactsList =
                contacts.readListOfContacts(number);

        student.setContacts(contactsList);
        return student;
    }

    public List<Student> readStudentList(int number) {
        List<Student> studentList = new ArrayList<>();
        for(int i = 0; i< number; i++) {
            studentList.add(readStudentData());
        }
        return studentList;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", rollNumber='" + rollNumber + '\'' +
                ", address=" + address +
                ", contacts=" + contacts +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {

        int intRoll = Integer.parseInt(rollNumber);
        if(intRoll < 0 || intRoll > 1000) {
            throw new IllegalArgumentException("Invalid roll number. Please enter not lar");
        }
        this.rollNumber = rollNumber;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Contacts> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contacts> contacts) {
        this.contacts = contacts;
    }
}
