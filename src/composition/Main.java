package composition;

/**
 * @author rajim 2022-04-25.
 * @project IntelliJ IDEA
 */
public class Main {
    public static void main(String[] args) {

        // read single student and print

        Student student = new Student();

        Student studentData = student.readStudentData();
        System.out.println("Student data::"
                +studentData.toString());



    }
}
