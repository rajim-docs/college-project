package composition;

import java.util.*;

/**
 * @author rajim 2022-04-25.
 * @project IntelliJ IDEA
 */
public class Contacts {

    private String mobileNumber;

    private String email;

    @Override
    public String toString() {
        return "Contacts{" +
                "mobileNumber='" + mobileNumber + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public Contacts readContact(){
        Scanner scanner = new Scanner(System.in);
        Contacts contacts = new Contacts();
        System.out.println("Enter mobile number");
        contacts.setMobileNumber(scanner.next());
        System.out.println("Enter Email::");
        contacts.setEmail(scanner.next());
        return contacts;
    }

    public List<Contacts> readListOfContacts(int number) {
        List<Contacts> contacts = new ArrayList<>();
        for(int i = 0; i< number; i++) {
            contacts.add(readContact());
        }
        return contacts;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
