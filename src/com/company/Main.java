package com.company;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;

/**
 * @author rajim 2022-04-18.
 * @project IntelliJ IDEA
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world");
        // TO read from console
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter number");
        int number = scanner.nextInt();

        // read multiple student records

        List<Student> studentList = new ArrayList<>();

        for(int i = 0; i<number; i++) {
            System.out.println("enter student name::");
            String name = scanner.next();
            System.out.println("enter roll number:");
            int rollNumber = scanner.nextInt();
            System.out.println("enter address::");
            String address = scanner.next();
            Student student = new Student(
                    name, rollNumber, address
            );
            studentList.add(student);
        }

        // printing list of student

        for( int i = 0; i< number; i++) {
            System.out.println(
                    studentList.get(i).toString());
        }

        for(Student student: studentList) {
            System.out.println(student.toString());
        }

    }

}
