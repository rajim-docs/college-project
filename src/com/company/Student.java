package com.company;

/**
 * @author rajim 2022-04-18.
 * @project IntelliJ IDEA
 */
public class Student {

    private String name;
    private int rollNumber;
    private String address;

    public Student() {

    }

    public Student(String name, int rollNumber, String address) {
        this.name = name;
        this.rollNumber = rollNumber;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", rollNumber=" + rollNumber +
                ", address='" + address + '\'' +
                '}';
    }
}
